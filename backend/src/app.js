// Express app with CORS, express.json(),
// ES6 import/export (using babel, no need for .js in import path)

import express from 'express';
import cors from 'cors';
import { createItem, deleteItem, readItems, updateItem } from './models/item';

const app = express();
app.use(cors());
app.use(express.json());

app.get('/api/message', (req, res) => {
  res.send({ message: 'Hello World!' });
});

// Endpoint pour récupérer tous les éléments
app.get('/api/items', async (req, res) => {
  try {
    const results = await readItems();
    res.json(results);
  } catch (error) {
    console.error('Error fetching items:', error);
    res.status(500).json({ message: 'Server error' });
  }
});

// Endpoint pour ajouter un élément
app.post('/api/items', async (req, res) => {
  const { name } = req.body;
  if (!name) {
    res.status(400).json({ message: 'Name is required' });
    return;
  }
  try {
    const newItem = await createItem(name);
    res.status(201).json(newItem);
  } catch (error) {
    console.error('Error adding item:', error);
    res.status(500).json({ message: 'Server error' });
  }
});

// Endpoint pour mettre à jour un élément
app.put('/api/items/:id', async (req, res) => {
  const { id } = req.params;
  const { name, done } = req.body;
  if (!name && done === undefined) {
    res.status(400).json({ message: 'Name or done is required' });
    return;
  }
  try {
    const updatedItem = await updateItem(id, { name, done });
    res.json(updatedItem);
  } catch (error) {
    console.error('Error updating item:', error);
    res.status(500).json({ message: 'Server error' });
  }
});

// Endpoint pour supprimer un élément
app.delete('/api/items/:id', async (req, res) => {
  const { id } = req.params;
  try {
    await deleteItem(id);
    res.sendStatus(204);
  } catch (error) {
    console.error('Error deleting item:', error);
    res.status(500).json({ message: 'Server error' });
  }
});

export default app;
